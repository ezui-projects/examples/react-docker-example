FROM node:20-alpine
USER root
WORKDIR /app/
COPY public/ /app/public
COPY src/ /app/src
COPY package.json /app/
RUN npm install
EXPOSE 3000
CMD ["npm", "start"]
