#!/bin/sh
export COMPOSE_FILE_PATH="${PWD}/docker-compose.yml"
build_images(){
  docker build --no-cache --label "builder=ezui" --tag ezui/tipo/examples/react-docker-example:1.0.0 ./
}

install_app(){
  docker stack deploy --compose-file ./docker-compose.yml example
}

rmimages() {
  docker image rm -f 
}

case "$1" in
  build_images)
    build_images
    ;;
  install)
    build_images
	install_app
    ;;
  purge)
    rmimages
    ;;
  *)
    echo "Usage: $0 {build_images|install|purge}"
esac